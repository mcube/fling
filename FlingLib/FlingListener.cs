﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace FlingLib
{
    public class FlingListener
    {
        private HttpListener _listener = null;
        private String _currentFileName = null;
        private String _currentClientName = null;
        private DateTime _lastUpdate = DateTime.Now;
        private TimeSpan _timeout = TimeSpan.MaxValue;

        private static JavaScriptSerializer _serializer = new JavaScriptSerializer();

        public FlingListener()
        {
            _listener = new HttpListener();
            _listener.Prefixes.Add("http://+:8081/");
            _listener.AuthenticationSchemes = AuthenticationSchemes.Anonymous;
        }

        public FlingListener(int timeoutSeconds) : this()
        {
            _timeout = TimeSpan.FromSeconds(timeoutSeconds);
        }

        public void Start()
        {
             // ThreadStart ts = new ThreadStart(Loop);
             // Thread workerThread = new Thread(ts);
             // workerThread.Start();
            Loop();
        }

        public void Stop()
        {
            _listener.Stop();
        }

        public void Loop()
        {
            try {
                _listener.Start();
                Console.WriteLine("Server listening...");
                while (_listener.IsListening)
                {
                    try
                    {
                        // var result = _listener.BeginGetContext(ListenerCallback, _listener);
                        // result.AsyncWaitHandle.WaitOne();
                        ListenerCallback(_listener.GetContext());
                    }
                    catch (Exception ex)
                    {
                        LogEntry(ex.ToString(), EventLogEntryType.Error);
                        Console.WriteLine(ex.ToString());
                    }
                }                        
            }
            catch (HttpListenerException ex)
            {
                LogEntry(ex.ToString(), EventLogEntryType.Error);
                Console.WriteLine(ex.ToString());
                Console.WriteLine("In caso di 'Access is denied', esegui qusto comando: 'netsh http add urlacl url=http://+:8081/ user=<username>'");
                _listener.Stop();
            }
        }

        public void Set(String fileName, String clientName)
        {
            this._currentClientName = clientName;
            this._currentFileName = fileName;
            _lastUpdate = DateTime.Now;
        }

        public void Set(String fileName)
        {
            this.Set(fileName, "<anonymous>");
        }

        //private void ListenerCallback(IAsyncResult result)
        //{
        //    var context = _listener.EndGetContext(result);
        private void ListenerCallback(HttpListenerContext context) 
        {

            HttpListenerRequest request = context.Request;
            HttpListenerResponse response = context.Response;

            if (request.Headers["Origin"] != null)
            {
                response.AddHeader("Access-Control-Allow-Origin", request.Headers["Origin"]);
                response.AddHeader("Access-Control-Allow-Methods", "GET, POST, DELETE");
            }

            switch (request.HttpMethod)
            {
                case "GET":
                    response.StatusCode = (int)HttpStatusCode.OK;
                    response.StatusDescription = HttpStatusCode.OK.ToString();

                    // if _currentFile is null or timeout expired
                    if (String.IsNullOrEmpty(_currentFileName) || _timeout < DateTime.Now.Subtract(_lastUpdate)) //_lastUpdate.Add(_timeout) < DateTime.Now)
                    {
                        if (request.AcceptTypes.Contains("application/json"))
                        {
                            // JSON
                            FlingResponse flingResponse = new FlingResponse { Available = true };
                            sendJson(flingResponse, response);
                        }
                        else
                        {
                            // TEXT
                            sendString("", response);
                        }
                    }
                    else // _currentFile not null and timeout not expired
                    {
                        if (request.AcceptTypes.Contains("application/json"))
                        {
                            // JSON                            
                            FlingResponse flingResponse = new FlingResponse { Available = false, ClientName = _currentClientName, FileName = _currentFileName };
                            sendJson(flingResponse, response);
                        }
                        else
                        {
                            // TEXT
                            sendString(_currentFileName, response);
                        }
                    }
                    break;
                case "OPTIONS":
                    response.StatusCode = (int)HttpStatusCode.OK;
                    response.StatusDescription = HttpStatusCode.OK.ToString();
                    response.OutputStream.Close();
                    break;
                case "DELETE":
                    Console.WriteLine("DELETE");
                    response.StatusCode = (int)HttpStatusCode.OK;
                    response.StatusDescription = HttpStatusCode.OK.ToString();
                    _currentFileName = "";
                    _currentClientName = "";
                    sendString("", response);
                    break;
                case "POST":

                    if (request.HasEntityBody)
                    {
                        // read body
                        Encoding encoding = request.ContentEncoding;
                        using (var bodyStream = request.InputStream)
                        using (var streamReader = new StreamReader(bodyStream, encoding))
                        {
                            String body = streamReader.ReadToEnd();
                            if (request.ContentType == "application/json" || request.ContentType.StartsWith("application/json"))
                            {
                                try
                                {
                                    FlingRequest flingRequest = _serializer.Deserialize<FlingRequest>(body);
                                    if (String.IsNullOrEmpty(flingRequest.FileName))
                                    {
                                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                                        response.StatusDescription = "Missing required parameter FileName";
                                        return;
                                    }
                                    else
                                    {
                                        _currentFileName = flingRequest.FileName;
                                        _currentClientName = flingRequest.ClientName;
                                        _lastUpdate = DateTime.Now;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                                    response.StatusDescription = "Expected: JSON";
                                    return;
                                }
                            }
                            else
                            {
                                _currentFileName = body;
                                _currentClientName = "<anonymous>";
                                _lastUpdate = DateTime.Now;
                            }
                        }

                    }
                    else if (!String.IsNullOrEmpty(request.Url.Query))
                    {
                        NameValueCollection queryString = request.QueryString;
                        string[] keys = queryString.AllKeys;
                        if (keys.Contains("FileName") && keys.Contains("ClientName"))
                        {
                            _currentFileName = queryString["FileName"];
                            _currentClientName = queryString["ClientName"];
                            _lastUpdate = DateTime.Now;
                        }
                        else
                        {
                            response.StatusCode = (int)HttpStatusCode.BadRequest;
                            response.StatusDescription = "Missing required parameters FileName, ClientName";
                            return;
                        }
                    }
                    else
                    {
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        response.StatusDescription = "Missing required parameters FileName, ClientName";
                        return;
                    }
                    sendString("", response);
                    Console.WriteLine("POST: {0}", _currentFileName);
                    break;
                default:
                    response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                    response.StatusDescription = HttpStatusCode.MethodNotAllowed.ToString();
                    sendString("", response);
                    break;
            }
        }

        private static void sendString(String str, HttpListenerResponse response)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(str + "\n");
            response.ContentType = "text/plain";
            response.ContentEncoding = Encoding.UTF8;
            response.ContentLength64 = buffer.Length;
            response.OutputStream.Write(buffer, 0, buffer.Length);
            response.OutputStream.Close();
        }

        private static void sendJson(Object obj, HttpListenerResponse response)
        {

            String serializedResult = _serializer.Serialize(obj);
            byte[] buffer = Encoding.UTF8.GetBytes(serializedResult + "\n");
            response.ContentType = "application/json";
            response.ContentEncoding = Encoding.UTF8;
            response.ContentLength64 = buffer.Length;
            response.OutputStream.Write(buffer, 0, buffer.Length);
            response.OutputStream.Close();
        }

        private static void LogEntry(string message, EventLogEntryType logType)
        {
            if (!EventLog.SourceExists("Fling")) EventLog.CreateEventSource("Fling", "Application");
            EventLog ncsLog = new EventLog();
            ncsLog.Source = "Fling";
            ncsLog.WriteEntry(message, logType);
        }

    }

    public class FlingResponse
    {
        public Boolean Available { get; set; }
        public String FileName { get; set; }
        public String ClientName { get; set; }
    }

    public class FlingRequest
    {
        public String FileName { get; set; }
        public String ClientName { get; set; }
    }



}
