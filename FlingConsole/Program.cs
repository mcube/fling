﻿using FlingLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlingConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            FlingListener flingListener = args.Length >= 1 ? new FlingListener(int.Parse(args[0])) : new FlingListener();


            flingListener.Start();
            // Console.Write("Press a key to exit... ");
            // Console.Read();
            // flingListener.Stop();


        }
    }
}
