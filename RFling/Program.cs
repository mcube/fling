﻿using FlingLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RFling
{
    class Program
    {
        static void Main(string[] args)
        {
            FlingListener flingListener = args.Length >= 1 ? new FlingListener(int.Parse(args[0])) : new FlingListener();
            flingListener.Start();

            flingListener.Set("merda");
        }
    }
}
