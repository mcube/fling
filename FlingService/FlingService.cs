﻿using FlingLib;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Fling
{
    public partial class FlingService : ServiceBase
    {

        private FlingListener flingListener;

        public FlingService()
        {
            InitializeComponent();
            flingListener = new FlingListener();
        }

        protected override void OnStart(string[] args)
        {
            flingListener.Start();
        }

        protected override void OnStop()
        {
            flingListener.Stop();
        }

    }
}